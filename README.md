# Informatique - Se défendre et Attaquer

![Couverture originale de la brochure](cover.jpg)

> Guide sur l'Informatique, sous l'angle de l'autodéfense numérique et de l'attaque légitime. Version remixé par La Quadrature du Net depuis la version disponible sur Infokiosques.net

---

> Cette brochure a été faite par désir de rassembler les connaissances théoriques et les outils pratiques actuellement les plus efficaces à nos yeux, pour utiliser l’informatique pour des activités sensibles, sans se faire avoir. Concrètement, ça implique d’être en mesure d’agir de manière anonyme, confidentielle et en laissant le moins de traces possible derrière nous. Sans ces précautions, inutile d’espérer déjouer longtemps la surveillance et la répression employées par les États et leurs classes dirigeantes pour continuer à exercer tranquillement leur domination.

> Se réapproprier les outils informatiques, c’est comprendre pour mieux se défendre et... attaquer, mais c’est aussi se donner les moyens de pouvoir choisir en connaissance de cause, quand ne pas utiliser l’informatique.

> Actualisé en juin 2015.

---

[📖 Version PDF originale](InformatiqueSeDefendreEtAttaquer-120pA5-fil.pdf)  
[🔗 Lien vers Infokiosques.net](https://infokiosques.net/spip.php?article1045)
